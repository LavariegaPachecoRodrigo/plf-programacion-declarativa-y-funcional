# Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
*[#5F9EA0] Programación Declarativa. \nOrientaciones y Pautas para \nel Estudio
	*_ Es
		*[#DodgerBlue] Un estilo de programación o paradigma
			*_ Surge como reaccion \n a problemas de:
				*[#SkyBlue] La programación clásica
	*_ Sus	
		*[#DodgerBlue] Caracteristicas
			*_ Son
				*[#SkyBlue] Enfasis en aspectos creativos
				*[#SkyBlue] El compilador se encarga de las tareas rutinarias
				*[#SkyBlue] Abandona las secuencias\nde ordenes que gestionan la memoria
	*_ Su
		*[#DodgerBlue] Proposito
			*_ Es
				*[#SkyBlue] Reducir la complejidad de los programas
				*[#SkyBlue] Reducir el riesgo de cometer errores al codificar
				*[#SkyBlue] Facilita la programacion
	*_ Sus
		*[#DodgerBlue] Ventajas
			*_ Son
				*[#SkyBlue] Obtener programas mas cortos y faciles de realizar
				*[#SkyBlue] Codigo facil de depurar
				*[#SkyBlue] Tiempo de desarrollo de una aplicacion\nmas aceptable
				*[#SkyBlue] El Tiempo de modificacion es mucho menor
				*[#SkyBlue] El tiempo de depuracion es mas rapido
	*_ Su
		*[#DodgerBlue] Enfoque
			*_ Es
				*[#SkyBlue] Desechar las asiganciones
				*[#SkyBlue] No tener que detallar especificamente el control \n la gestion de memoria
				*[#SkyBlue] Utilizar recursos expresivos
				*[#SkyBlue] Adaptar los programas a la forma de pensar dle progrmador
	*_ Evolucion de 
		*[#DodgerBlue] Lenguajes de programacion
			*_ A traves de 
				*[#SkyBlue] Comunicacion con codigo mauqina con las computadoras
				*[#SkyBlue] Los programas se componen de secuencias de ordenes directas
				*[#SkyBlue] Se crearon lenguajes de alto nivel
					*_ Como 
						*[#LightBlue] Fortran
	*_ Sus
		*[#DodgerBlue] Variantes
			*_ son
				*[#SkyBlue] Programacion Funcional
					*_ se basa
						*[#LightBlue] Razonamiento ecuacional
					*_ emplea
						*[#LightBlue] El lenguaje de los matematicos
					*_ sus 
						*[#LightBlue] Caracteristicas
							*_ son 
								*[#LightBlue] Describe funciones	
								*[#LightBlue] Capacidad Expresiva	
								*[#LightBlue] Suceciones infinitas				
								*[#LightBlue] Utiliza Evaluacion perezosa						
					*_ sus
						*[#Silver] Ventajas	
							*_ son 
								*[#Silver] Es posible usar funciones de orden superior	
								*[#Silver] Contiene reglas de simplificacion
				*[#SkyBlue] Programacion Logica
					*_ establece
						*[#Silver] Relacion entre objetos
					*_ recurre
						*[#Silver] A Logica de predicados de primer orden
					*_ sus 
						*[#Silver] Caracteristicas
							*_ son 
								*[#Silver] No establece orden entre los argumentos
								*[#Silver] Parde de hechos o reglas	
								*[#Silver] Hace inferencias									
					*_ sus
						*[#Silver] Ventajas	
							*_ son 
								*[#Silver] Permite ser mas declarativo	
								*[#Silver] Busca oisibles soluciones
@endmindmap
```

# Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
*[#Crimson] PROGRAMACION FUNCIONAL
	*_ Es
		*[#DarkSeaGreen] Un paradigma de programacion
			*_ tiene
				*[#Beige] Modelos de computacion
				*[#Beige] Modelos de VonNeuman
	*_ tiene	
		*[#DarkSeaGreen] Caracteristicas
			*_ Las cuales Son
				*[#DarkSeaGreen] Funciones
					*_ reciben 
						*[#SandyBrown] Otra funcion como parametro de entrada
					*_ pueden
						*[#SandyBrown] Ser parametros de otras funciones
				*[#DarkSeaGreen] Currificacion
					*_ es
						*[#SandyBrown] Una funcion de varios parametros de entrada
							*_ devuelve
								*[#ad8585] Una funcion de varios parametros de entrada
				*[#DarkSeaGreen] Memorizacion
					*_ almacena
						*[#SandyBrown] El valor de la expresion que ya haya sido evaluada
					*_ en donde
						*[#SandyBrown] Solo se evalua una unica vez
					*_ se conoce como
						*[#SandyBrown] Paso por necesidad
				*[#DarkSeaGreen] Evaluacion
					*_ en
						*[#SandyBrown] Lenguajes imperativos
							*_ funcionamiento
								*[#SandyBrown] Evalua de dentro hacia afuera
							*_ solo
								*[#SandyBrown] Realiza calculos necesarios
					*_ de la
						*[#SandyBrown] Solo se evalua una unica vez
	*_ sus
		*[#DarkSeaGreen] Ventajas
			*_ Son
				*[#SandyBrown] Las funcones pueden estar en todos lados
				*[#SandyBrown] Los resultados son idependientes del orden del calculo
				*[#SandyBrown] Los programas solo dependen dle parametro de entrada
				*[#SandyBrown] Permitir escribir codigo mas rapido
				*[#SandyBrown] Es posible paralizar las funciones
	*_ Sus
		*[#DarkSeaGreen] Bases 
			*_ Son
				*[#SandyBrown] El programa es una Funcion matematica
				*[#SandyBrown] La variable
					*_ se utiliza
						*[#ad8585] Para referirse a los parametros de entrada de las funciones
					*_ solo
						*[#ad8585] Se utilizan para el Almacenamiento temrporal.
					*_ pierden
						*[#ad8585] El concepto zona modificable de memoria
						*[#ad8585] El uso de bucles
	*_ posee
		*[#DarkSeaGreen] Variantes
			*_ como
				*[#SandyBrown] Programacion Orientada a Objetos
					*_ son
						*[#SandyBrown] Pequeños fragmentos de codigo dond elos objetos interactuan entre si
						*[#SandyBrown] Se compone de instrucciones ejecutadas secuecialmente
				*[#SandyBrown] Logica Simbolica		
					*_ son
						*[#SandyBrown] Conjuto de secuencias
							*_ definen
								*[#ad8585] Lo que es verdad
								*[#ad8585] Lo que es conocido
							*_ utiliza
								*[#ad8585] Inferencia logica para Resolver problemas
						*[#SandyBrown] Funcionamiento
	*_ El
		*[#DarkSeaGreen] Lambda Calculo
			*_ En 
				*[#SandyBrown]  1930
			*_ Sistema
				*[#SandyBrown] Potente equivalente al modelo de John Neumann
				*[#SandyBrown] Para estudiar funcion, recursividad
			*_ La 
				*[#SandyBrown] Currificacion
					*_ es 
						*[#ad8585] Una funcion con n parametros y devuelve un \nparametro de salida que puede ser\nvista como una funcion que tiene un \nunico parametro de entrada	
						*[#ad8585] Aplicacion parcial
	*_ La
		*[#DarkSeaGreen] Programacion Imperativa
			*_ se
				*[#ad8585] Deben dar ordenes secuenciales
			*_ tiene 
				*[#ad8585] Instruccion de asignacion
			*_ En una funcion puede haber
				*[#ad8585] efectos colaterales
				*[#ad8585] mayor riesgo de cometer errores
				*[#ad8585] modificaciones en ele estado del computo parlante
			*_ Evaluacion
				*[#ad8585] En cortocircuito McCarthy	
@endmindmap
```


